package app.dummy.com.viewmodels;

import android.app.Application;

import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import app.dummy.com.repositories.AuthRepository;
import app.dummy.com.retrofit.observers.RetrofitObserver;
import app.dummy.com.room.models.User;

public class AuthViewModel extends AndroidViewModel {
    private AuthRepository authRepository;

    public AuthViewModel(Application application) {
        super(application);
        authRepository = AuthRepository.getInstance(application);
    }

    public LiveData<User> findCurrentUser() {
        return authRepository.findCurrentUser();
    }

    public RetrofitObserver identification(String phoneNumber) {
        return authRepository.identification(phoneNumber);
    }

    public RetrofitObserver register(String phoneNumber, String fullName, String password, String passwordConfirmation) {
        return authRepository.register(phoneNumber, fullName, password, passwordConfirmation);
    }

    public RetrofitObserver login(String phoneNumber, String password) {
        return authRepository.login(phoneNumber, password);
    }
}
