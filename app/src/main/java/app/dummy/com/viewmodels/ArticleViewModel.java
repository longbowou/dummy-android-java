package app.dummy.com.viewmodels;

import android.app.Application;

import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import java.util.List;

import app.dummy.com.repositories.ArticleRepository;
import app.dummy.com.retrofit.observers.RetrofitObserver;
import app.dummy.com.room.models.Article;
import app.dummy.com.room.models.Comment;

public class ArticleViewModel extends AndroidViewModel {
    private ArticleRepository articleRepository;

    public ArticleViewModel(Application application) {
        super(application);
        articleRepository = ArticleRepository.getInstance(application);
    }

    public LiveData<Article> find(Long articleId) {
        return articleRepository.find(articleId);
    }

    public LiveData<List<Article>> findAll() {
        return articleRepository.findAll();
    }

    public LiveData<List<Article>> findBookmarked(Long userId) {
        return articleRepository.findBookmarked(userId);
    }

    public LiveData<List<Article>> findMyArticle(Long userId) {
        return articleRepository.findMyArticle(userId);
    }

    public LiveData<List<Comment>> findCommentsForArticle(Long articleId) {
        return articleRepository.findCommentsForArticle(articleId);
    }

    public RetrofitObserver fetchArticles(String atPage) {
        return articleRepository.fetchArticles(atPage);
    }

    public RetrofitObserver createArticle(String phoneNumber, String encryptedPassword, String articleTitle, String articleDescription, String articleImageUrl) {
        return articleRepository.createArticle(phoneNumber, encryptedPassword, articleTitle, articleDescription, articleImageUrl);
    }

    public RetrofitObserver commentArticle(String phoneNumber, String encryptedPassword, Long articleId, String commentContent) {
        return articleRepository.commentArticle(phoneNumber, encryptedPassword, articleId, commentContent);
    }

    public RetrofitObserver bookmarkUnBookmarkArticle(String phoneNumber, String encryptedPassword, Long articleId) {
        return articleRepository.bookmarkUnBookmarkArticle(phoneNumber, encryptedPassword, articleId);
    }

    public RetrofitObserver likeDislikeArticle(String phoneNumber, String encryptedPassword, Long articleId) {
        return articleRepository.likeDislikeArticle(phoneNumber, encryptedPassword, articleId);
    }

    public RetrofitObserver fetchUserBookmarks(String phoneNumber, String encryptedPassword) {
        return articleRepository.fetchUserBookmarks(phoneNumber, encryptedPassword);
    }

    public RetrofitObserver fetchArticleComments(Long articleId, String atPage) {
        return articleRepository.fetchArticleComments(articleId, atPage);
    }
}
