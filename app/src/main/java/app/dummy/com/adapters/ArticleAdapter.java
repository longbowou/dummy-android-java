package app.dummy.com.adapters;

import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import app.dummy.com.R;
import app.dummy.com.activities.ArticleDetailsActivity;
import app.dummy.com.retrofit.observers.RetrofitListener;
import app.dummy.com.room.models.Article;
import app.dummy.com.room.models.User;
import app.dummy.com.viewmodels.ArticleViewModel;

public class ArticleAdapter extends RecyclerView.Adapter<ArticleAdapter.ArticleViewHolder> {

    private List<Article> articles = new ArrayList<>();
    private User currentUser;
    private ArticleViewModel articleViewModel;

    public ArticleAdapter(User currentUser, ArticleViewModel articleViewModel) {
        this.currentUser = currentUser;
        this.articleViewModel = articleViewModel;
    }

    @NonNull
    @Override
    public ArticleViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.article_item, parent, false);

        return new ArticleViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ArticleViewHolder holder, int position) {
        final Article currentArticle = articles.get(position);

        holder.articleTitle.setText(currentArticle.getTitle());
        holder.articleDescription.setText(currentArticle.getDescription());
        holder.articleCreatedAt.setText(currentArticle.getCreatedAtLabel());
        holder.articleAuthor.setText(currentArticle.getAuthor());
        holder.articleCommentsCount.setText(currentArticle.getCommentsCount() + "");
        holder.articleLikesCount.setText(currentArticle.getLikesCount() + "");

        for (String id : currentArticle.getUsersLikesIds().split(" ")) {
            if (!id.equals("") && !id.equals(" ") && Long.valueOf(id).equals(currentUser.getId())) {
                holder.likeButton.setImageDrawable(holder.itemView.getContext().getResources().getDrawable(R.drawable.ic_favorite_green_24dp));
                break;
            } else {
                holder.likeButton.setImageDrawable(holder.itemView.getContext().getResources().getDrawable(R.drawable.ic_favorite_black_24dp));
            }
        }

        for (String id : currentArticle.getUsersBookmarksIds().split(" ")) {
            if (!id.equals("") && !id.equals(" ") && Long.valueOf(id).equals(currentUser.getId())) {
                holder.bookmarkButton.setImageDrawable(holder.itemView.getContext().getResources().getDrawable(R.drawable.ic_bookmark_green_24dp));
            } else {
                holder.bookmarkButton.setImageDrawable(holder.itemView.getContext().getResources().getDrawable(R.drawable.ic_bookmark_black_24dp));
            }
        }

        holder.likeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                articleViewModel.likeDislikeArticle(currentUser.getPhoneNumber(), currentUser.getEncryptedPassword(), currentArticle.getId()).observe(new RetrofitListener() {
                    @Override
                    public void onSuccess(String message) {

                    }
                });
            }
        });

        holder.bookmarkButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                articleViewModel.bookmarkUnBookmarkArticle(currentUser.getPhoneNumber(), currentUser.getEncryptedPassword(), currentArticle.getId()).observe(new RetrofitListener() {
                    @Override
                    public void onSuccess(String message) {
                    }
                });
            }
        });

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                holder.itemView.getContext().startActivity((new Intent(holder.itemView.getContext(), ArticleDetailsActivity.class)).putExtra(ArticleDetailsActivity.ARG_ARTICLE_ID, currentArticle.getId()));
            }
        });
    }

    @Override
    public int getItemCount() {
        return articles.size();
    }

    public void update(List<Article> articles) {
        this.articles = articles;
        notifyDataSetChanged();
    }

    public class ArticleViewHolder extends RecyclerView.ViewHolder {

        public ImageView articleImage;
        public TextView articleTitle;
        public TextView articleDescription;
        public TextView articleCreatedAt;
        public TextView articleAuthor;
        public TextView articleCommentsCount;
        public TextView articleLikesCount;
        public ImageButton commentButton;
        public ImageButton likeButton;
        public ImageButton bookmarkButton;

        public ArticleViewHolder(@NonNull View itemView) {
            super(itemView);
            articleImage = itemView.findViewById(R.id.article_image);
            articleTitle = itemView.findViewById(R.id.article_title);
            articleDescription = itemView.findViewById(R.id.article_description);
            articleCreatedAt = itemView.findViewById(R.id.article_created_at);
            articleAuthor = itemView.findViewById(R.id.article_author);
            articleCommentsCount = itemView.findViewById(R.id.article_comments_count);
            articleLikesCount = itemView.findViewById(R.id.article_likes_count);
            commentButton = itemView.findViewById(R.id.commentButton);
            likeButton = itemView.findViewById(R.id.likeButton);
            bookmarkButton = itemView.findViewById(R.id.bookmarkButton);
        }
    }
}