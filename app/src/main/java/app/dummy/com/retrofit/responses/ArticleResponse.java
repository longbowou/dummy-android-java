package app.dummy.com.retrofit.responses;

import app.dummy.com.room.models.Article;
import app.dummy.com.room.models.User;

public class ArticleResponse extends Response {

    private Article article;

    private User user;

    public Article getArticle() {
        return article;
    }

    public void setArticle(Article article) {
        this.article = article;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
