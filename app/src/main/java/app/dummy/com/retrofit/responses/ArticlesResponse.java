package app.dummy.com.retrofit.responses;

import java.util.List;

import app.dummy.com.room.models.Article;

public class ArticlesResponse extends Response {

    private List<Article> articles;

    public List<Article> getArticles() {
        return articles;
    }

    public void setArticles(List<Article> articles) {
        this.articles = articles;
    }
}
