package app.dummy.com.retrofit.responses;

import app.dummy.com.room.models.Article;
import app.dummy.com.room.models.Comment;

public class CommentArticleResponse extends Response {

    private Article article;

    private Comment comment;

    public Article getArticle() {
        return article;
    }

    public void setArticle(Article article) {
        this.article = article;
    }

    public Comment getComment() {
        return comment;
    }

    public void setComment(Comment comment) {
        this.comment = comment;
    }
}
