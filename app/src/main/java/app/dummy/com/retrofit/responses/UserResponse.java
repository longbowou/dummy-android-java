package app.dummy.com.retrofit.responses;

import app.dummy.com.room.models.User;

public class UserResponse extends Response {

    private User user;

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
