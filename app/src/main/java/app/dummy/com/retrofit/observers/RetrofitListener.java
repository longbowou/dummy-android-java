package app.dummy.com.retrofit.observers;

public abstract class RetrofitListener {

    public void onSuccess() {
    }

    public void onSuccess(String message) {
    }

    public void onSuccess(int responseStatus, String message) {
    }

    public void onFailure() {
    }
}
