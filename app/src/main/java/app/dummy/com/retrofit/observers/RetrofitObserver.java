package app.dummy.com.retrofit.observers;

public class RetrofitObserver {

    public RetrofitListener retrofitListener;

    public void observe(RetrofitListener retrofitListener) {
        this.retrofitListener = retrofitListener;
    }
}
