package app.dummy.com.retrofit;

import app.dummy.com.retrofit.responses.ArticleResponse;
import app.dummy.com.retrofit.responses.ArticlesResponse;
import app.dummy.com.retrofit.responses.CommentArticleResponse;
import app.dummy.com.retrofit.responses.CommentsResponse;
import app.dummy.com.retrofit.responses.Response;
import app.dummy.com.retrofit.responses.UserResponse;
import app.dummy.com.room.models.Article;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface ApiService {

    @FormUrlEncoded
    @POST("users/identification")
    Call<Response> identification(@Field("phone_number") String phoneNumber);

    @FormUrlEncoded
    @POST("users/register")
    Call<UserResponse> register(@Field("phone_number") String phoneNumber, @Field("full_name") String fullName, @Field("password") String password, @Field("password_confirmation") String passwordConfirmation);

    @FormUrlEncoded
    @POST("users/login")
    Call<UserResponse> login(@Field("phone_number") String phoneNumber, @Field("password") String password);

    @FormUrlEncoded
    @POST("users/articles/submit")
    Call<ArticleResponse> createArticle(@Field("phone_number") String phoneNumber, @Field("encrypted_password") String encryptedPassword, @Field("article_title") String articleTitle, @Field("article_description") String articleDescription, @Field("article_image_url") String articleImageUrl);

    @FormUrlEncoded
    @POST("users/articles/comments/submit")
    Call<CommentArticleResponse> commentArticle(@Field("phone_number") String phoneNumber, @Field("encrypted_password") String encryptedPassword, @Field("article_id") Long articleId, @Field("comment_content") String commentContent);

    @FormUrlEncoded
    @POST("users/articles/bookmark_un_bookmark")
    Call<ArticleResponse> bookmarkUnBookmarkArticle(@Field("phone_number") String phoneNumber, @Field("encrypted_password") String encryptedPassword, @Field("article_id") Long articleId);

    @FormUrlEncoded
    @POST("users/articles/like_dislike")
    Call<ArticleResponse> likeDislikeArticle(@Field("phone_number") String phoneNumber, @Field("encrypted_password") String encryptedPassword, @Field("article_id") Long articleId);

    @FormUrlEncoded
    @POST("users/account/bookmarks")
    Call<ArticlesResponse> fetchUserBookmarks(@Field("phone_number") String phoneNumber, @Field("encrypted_password") String encryptedPassword);

    @GET("articles")
    Call<ArticlesResponse> fetchArticles(@Query("at_page") String at_page);

    @GET("articles/{article_id}/comments")
    Call<CommentsResponse> fetchArticleComments(@Path("article_id") Long articleId, @Query("at_page") String at_page);
}