package app.dummy.com.retrofit.responses;

import java.util.List;

import app.dummy.com.room.models.Comment;

public class CommentsResponse extends Response {

    private List<Comment> comments;

    public List<Comment> getComments() {
        return comments;
    }

    public void setComments(List<Comment> comments) {
        this.comments = comments;
    }
}
