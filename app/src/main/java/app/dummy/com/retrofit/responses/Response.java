package app.dummy.com.retrofit.responses;

import com.google.gson.annotations.SerializedName;

public class Response {
    @SerializedName("response_status")
    private int responseStatus;

    @SerializedName("message")
    private String message;

    public int getResponseStatus() {
        return responseStatus;
    }

    public void setResponseStatus(int responseStatus) {
        this.responseStatus = responseStatus;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
