package app.dummy.com.fragments;


import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import app.dummy.com.R;
import app.dummy.com.adapters.ArticleAdapter;
import app.dummy.com.room.models.Article;
import app.dummy.com.room.models.User;
import app.dummy.com.viewmodels.ArticleViewModel;
import app.dummy.com.viewmodels.AuthViewModel;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link MyArticlesFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class MyArticlesFragment extends Fragment {

    private RecyclerView articlesRecyclerView;
    private ArticleAdapter articleAdapter;
    private ArticleViewModel articleViewModel;
    private AuthViewModel authViewModel;


    public MyArticlesFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment MyArticlesFragment.
     */
    public static MyArticlesFragment newInstance() {
        MyArticlesFragment fragment = new MyArticlesFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_my_articles, container, false);

        articleViewModel = ViewModelProviders.of(this).get(ArticleViewModel.class);
        authViewModel = ViewModelProviders.of(this).get(AuthViewModel.class);
        authViewModel.findCurrentUser().observe(this, new Observer<User>() {
            @Override
            public void onChanged(User user) {
                if (user != null) {
                    setUpAdapter(user);
                }
                authViewModel.findCurrentUser().removeObserver(this);
            }
        });

        articlesRecyclerView = view.findViewById(R.id.articles_recycle_view);

        return view;
    }

    private void setUpAdapter(User user) {
        articleAdapter = new ArticleAdapter(user, articleViewModel);
        articlesRecyclerView.setAdapter(articleAdapter);
        articlesRecyclerView.setHasFixedSize(true);
        articlesRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));

        articleViewModel.findMyArticle(user.getId()).observe(this, new Observer<List<Article>>() {
            @Override
            public void onChanged(List<Article> articles) {
                articleAdapter.update(articles);
            }
        });
    }
}
