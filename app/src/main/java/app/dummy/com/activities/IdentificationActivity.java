package app.dummy.com.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProviders;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import app.dummy.com.R;
import app.dummy.com.retrofit.observers.RetrofitListener;
import app.dummy.com.viewmodels.AuthViewModel;
import cn.pedant.SweetAlert.SweetAlertDialog;

public class IdentificationActivity extends AppCompatActivity {

    private FloatingActionButton floatingActionButton;
    private EditText phoneNumberEditText;
    private AuthViewModel authViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_identification);

        floatingActionButton = findViewById(R.id.floatingActionButton);
        phoneNumberEditText = findViewById(R.id.phone_number);

        authViewModel = ViewModelProviders.of(this).get(AuthViewModel.class);

        floatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (phoneNumberEditText.getText().toString().trim().equals("")) {
                    phoneNumberEditText.setError("Please fill a phone number.");
                    return;
                }
                phoneNumberEditText.setError(null);

                identification(phoneNumberEditText.getText().toString());
            }
        });
    }

    private void identification(final String phoneNumber) {
        final SweetAlertDialog pDialog = new SweetAlertDialog(this, SweetAlertDialog.PROGRESS_TYPE);
        pDialog.getProgressHelper().setBarColor(Color.parseColor("#D81B60"));
        pDialog.setTitleText("Loading");
        pDialog.setCancelable(false);
        pDialog.show();

        authViewModel.identification(phoneNumber).observe(new RetrofitListener() {
            @Override
            public void onSuccess(int responseStatus, String message) {
                pDialog.dismiss();

                if (responseStatus == 200) {
                    SweetAlertDialog pDialog = new SweetAlertDialog(IdentificationActivity.this, SweetAlertDialog.SUCCESS_TYPE)
                            .setContentText(message)
                            .setConfirmText("Ok")
                            .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sweetAlertDialog) {
                                    startActivity((new Intent(getApplicationContext(), LoginActivity.class)).putExtra(LoginActivity.ARG_FULL_PHONE_NUMBER, phoneNumber));
                                }
                            });

                    pDialog.setCancelable(false);
                    pDialog.show();
                }

                if (responseStatus == 201) {
                    SweetAlertDialog pDialog = new SweetAlertDialog(IdentificationActivity.this, SweetAlertDialog.SUCCESS_TYPE)
                            .setContentText(message)
                            .setConfirmText("Ok")
                            .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sweetAlertDialog) {
                                    startActivity((new Intent(getApplicationContext(), RegisterActivity.class)).putExtra(RegisterActivity.ARG_FULL_PHONE_NUMBER, phoneNumber));
                                }
                            });

                    pDialog.setCancelable(false);
                    pDialog.show();
                }

                if (responseStatus == 400) {
                    new SweetAlertDialog(IdentificationActivity.this, SweetAlertDialog.ERROR_TYPE)
                            .setContentText(message)
                            .show();

                    phoneNumberEditText.setError(message);
                }
            }

            @Override
            public void onFailure() {
                pDialog.dismiss();

                new SweetAlertDialog(IdentificationActivity.this, SweetAlertDialog.ERROR_TYPE)
                        .setContentText("Oops network error. Please try again.")
                        .setConfirmText("Try again")
                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sweetAlertDialog) {
                                identification(phoneNumber);
                            }
                        })
                        .show();
            }
        });
    }
}
