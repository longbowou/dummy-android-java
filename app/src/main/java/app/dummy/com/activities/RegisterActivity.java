package app.dummy.com.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProviders;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import app.dummy.com.R;
import app.dummy.com.retrofit.observers.RetrofitListener;
import app.dummy.com.viewmodels.AuthViewModel;
import cn.pedant.SweetAlert.SweetAlertDialog;

public class RegisterActivity extends AppCompatActivity {

    public static final String ARG_FULL_PHONE_NUMBER = "ARG_FULL_PHONE_NUMBER";

    private TextView fullPhoneNumberTextView;
    private EditText fullNameEditText;
    private EditText passwordEditText;
    private EditText passwordConfirmationEditText;
    private FloatingActionButton floatingActionButton;
    private AuthViewModel authViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        authViewModel = ViewModelProviders.of(this).get(AuthViewModel.class);

        fullPhoneNumberTextView = findViewById(R.id.full_phone_number);
        fullNameEditText = findViewById(R.id.full_name);
        passwordEditText = findViewById(R.id.password);
        passwordConfirmationEditText = findViewById(R.id.password_confirmation);
        floatingActionButton = findViewById(R.id.floatingActionButton);

        fullPhoneNumberTextView.setText(getIntent().getExtras().getString(ARG_FULL_PHONE_NUMBER));
        floatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (fullNameEditText.getText().toString().trim().equals("")) {
                    fullNameEditText.setError("This file is required.");
                    return;
                }

                if (passwordEditText.getText().toString().equals("")) {
                    passwordEditText.setError("This file is required.");
                    return;
                }

                if (passwordEditText.getText().toString().length() < 6) {
                    passwordEditText.setError("The password must contains at least tree characters.");
                    return;
                }

                if (passwordConfirmationEditText.getText().toString().equals("")) {
                    passwordConfirmationEditText.setError("This file is required.");
                    return;
                }

                if (!passwordConfirmationEditText.getText().toString().equals(passwordEditText.getText().toString())) {
                    passwordEditText.requestFocus();
                    passwordEditText.setError("The and password confirmation d'ont match.");
                    return;
                }

                fullNameEditText.setError(null);
                passwordEditText.setError(null);
                passwordConfirmationEditText.setError(null);

                register(fullPhoneNumberTextView.getText().toString(), fullNameEditText.getText().toString(), passwordEditText.getText().toString(), passwordConfirmationEditText.getText().toString());
            }
        });
    }

    private void register(final String phoneNumber, final String fullName, final String password, final String passwordConfirmation) {
        final SweetAlertDialog pDialog = new SweetAlertDialog(this, SweetAlertDialog.PROGRESS_TYPE);
        pDialog.getProgressHelper().setBarColor(Color.parseColor("#D81B60"));
        pDialog.setTitleText("Loading");
        pDialog.setCancelable(false);
        pDialog.show();

        authViewModel.register(phoneNumber, fullName, password, passwordConfirmation).observe(new RetrofitListener() {
            @Override
            public void onSuccess(int responseStatus, String message) {
                pDialog.dismiss();

                if (responseStatus == 200) {
                    getSharedPreferences(MainActivity.SHARED_PREFERENCES_KEY, Context.MODE_PRIVATE).edit().putBoolean(MainActivity.IS_FIRST_LAUNCH, false).commit();

                    SweetAlertDialog pDialog = new SweetAlertDialog(RegisterActivity.this, SweetAlertDialog.SUCCESS_TYPE)
                            .setContentText(message)
                            .setConfirmText("Ok")
                            .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sweetAlertDialog) {
                                    startActivity((new Intent(getApplicationContext(), MainActivity.class)));
                                }
                            });

                    pDialog.setCancelable(false);
                    pDialog.show();
                }

                if (responseStatus == 400) {
                    new SweetAlertDialog(RegisterActivity.this, SweetAlertDialog.ERROR_TYPE)
                            .setContentText(message)
                            .show();
                }
            }

            @Override
            public void onFailure() {
                pDialog.dismiss();

                new SweetAlertDialog(RegisterActivity.this, SweetAlertDialog.ERROR_TYPE)
                        .setContentText("Oops network error. Please try again.")
                        .setConfirmText("Try again")
                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sweetAlertDialog) {
                                register(phoneNumber, fullName, password, passwordConfirmation);
                            }
                        })
                        .show();
            }
        });
    }
}
