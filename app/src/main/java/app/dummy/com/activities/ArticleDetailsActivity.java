package app.dummy.com.activities;

import android.os.Bundle;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import app.dummy.com.R;
import app.dummy.com.retrofit.observers.RetrofitListener;
import app.dummy.com.room.models.Article;
import app.dummy.com.room.models.User;
import app.dummy.com.viewmodels.ArticleViewModel;
import app.dummy.com.viewmodels.AuthViewModel;

public class ArticleDetailsActivity extends AppCompatActivity {

    public static final String ARG_ARTICLE_ID = "ARG_ARTICLE_ID";

    private ImageView articleImage;
    private TextView articleTitle;
    private TextView articleDescription;
    private TextView articleCreatedAt;
    private TextView articleAuthor;
    private TextView articleCommentsCount;
    private TextView articleLikesCount;
    private ImageButton commentButton;
    private ImageButton likeButton;
    private ImageButton bookmarkButton;
    private AuthViewModel authViewModel;
    private ArticleViewModel articleViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_article_details);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        authViewModel = ViewModelProviders.of(this).get(AuthViewModel.class);
        articleViewModel = ViewModelProviders.of(this).get(ArticleViewModel.class);

        articleImage = findViewById(R.id.article_image);
        articleTitle = findViewById(R.id.article_title);
        articleDescription = findViewById(R.id.article_description);
        articleCreatedAt = findViewById(R.id.article_created_at);
        articleAuthor = findViewById(R.id.article_author);
        articleCommentsCount = findViewById(R.id.article_comments_count);
        articleLikesCount = findViewById(R.id.article_likes_count);
        commentButton = findViewById(R.id.commentButton);
        likeButton = findViewById(R.id.likeButton);
        bookmarkButton = findViewById(R.id.bookmarkButton);

        authViewModel.findCurrentUser().observe(this, new Observer<User>() {
            @Override
            public void onChanged(final User user) {
                if (user != null) {
                    updateUI(user);
                }

                authViewModel.findCurrentUser().removeObserver(this);
            }
        });
    }

    private void updateUI(final User currentUser) {
        articleViewModel.find(getIntent().getExtras().getLong(ARG_ARTICLE_ID)).observe(this, new Observer<Article>() {
            @Override
            public void onChanged(final Article article) {
                articleTitle.setText(article.getTitle());
                articleDescription.setText(article.getDescription());
                articleCreatedAt.setText(article.getCreatedAtLabel());
                articleAuthor.setText(article.getAuthor());
                articleCommentsCount.setText(article.getCommentsCount() + "");
                articleLikesCount.setText(article.getLikesCount() + "");

                for (String id : article.getUsersLikesIds().split(" ")) {
                    if (!id.equals("") && !id.equals(" ") && Long.valueOf(id).equals(currentUser.getId())) {
                        likeButton.setImageDrawable(getApplicationContext().getResources().getDrawable(R.drawable.ic_favorite_green_24dp));
                        break;
                    } else {
                        likeButton.setImageDrawable(getApplicationContext().getResources().getDrawable(R.drawable.ic_favorite_black_24dp));
                    }
                }

                for (String id : article.getUsersBookmarksIds().split(" ")) {
                    if (!id.equals("") && !id.equals(" ") && Long.valueOf(id).equals(currentUser.getId())) {
                        bookmarkButton.setImageDrawable(getApplicationContext().getResources().getDrawable(R.drawable.ic_bookmark_green_24dp));
                    } else {
                        bookmarkButton.setImageDrawable(getApplicationContext().getResources().getDrawable(R.drawable.ic_bookmark_black_24dp));
                    }
                }

                likeButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        articleViewModel.likeDislikeArticle(currentUser.getPhoneNumber(), currentUser.getEncryptedPassword(), article.getId()).observe(new RetrofitListener() {
                            @Override
                            public void onSuccess(String message) {

                            }
                        });
                    }
                });

                bookmarkButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        articleViewModel.bookmarkUnBookmarkArticle(currentUser.getPhoneNumber(), currentUser.getEncryptedPassword(), article.getId()).observe(new RetrofitListener() {
                            @Override
                            public void onSuccess(String message) {
                            }
                        });
                    }
                });
            }
        });
    }
}
