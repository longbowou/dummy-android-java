package app.dummy.com.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProviders;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import app.dummy.com.R;
import app.dummy.com.retrofit.observers.RetrofitListener;
import app.dummy.com.viewmodels.AuthViewModel;
import cn.pedant.SweetAlert.SweetAlertDialog;

public class LoginActivity extends AppCompatActivity {

    public static final String ARG_FULL_PHONE_NUMBER = "ARG_FULL_PHONE_NUMBER";

    private TextView fullPhoneNumberTextView;
    private EditText passwordEditText;
    private AuthViewModel authViewModel;
    private FloatingActionButton floatingActionButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        authViewModel = ViewModelProviders.of(this).get(AuthViewModel.class);

        fullPhoneNumberTextView = findViewById(R.id.full_phone_number);
        passwordEditText = findViewById(R.id.password);
        floatingActionButton = findViewById(R.id.floatingActionButton);

        fullPhoneNumberTextView.setText(getIntent().getExtras().getString(ARG_FULL_PHONE_NUMBER));

        floatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (passwordEditText.getText().toString().equals("")) {
                    passwordEditText.setError("This file is required.");
                    return;
                }

                passwordEditText.setError(null);

                login(fullPhoneNumberTextView.getText().toString(), passwordEditText.getText().toString());
            }
        });
    }

    private void login(final String phoneNumber, final String password) {
        final SweetAlertDialog pDialog = new SweetAlertDialog(this, SweetAlertDialog.PROGRESS_TYPE);
        pDialog.getProgressHelper().setBarColor(Color.parseColor("#D81B60"));
        pDialog.setTitleText("Loading");
        pDialog.setCancelable(false);
        pDialog.show();

        authViewModel.login(phoneNumber, password).observe(new RetrofitListener() {
            @Override
            public void onSuccess(int responseStatus, String message) {
                pDialog.dismiss();

                if (responseStatus == 200) {
                    getSharedPreferences(MainActivity.SHARED_PREFERENCES_KEY, Context.MODE_PRIVATE).edit().putBoolean(MainActivity.IS_FIRST_LAUNCH, false).commit();

                    SweetAlertDialog pDialog = new SweetAlertDialog(LoginActivity.this, SweetAlertDialog.SUCCESS_TYPE)
                            .setContentText(message)
                            .setConfirmText("Ok")
                            .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sweetAlertDialog) {
                                    startActivity((new Intent(getApplicationContext(), MainActivity.class)));
                                }
                            });

                    pDialog.setCancelable(false);
                    pDialog.show();
                }

                if (responseStatus == 400) {
                    new SweetAlertDialog(LoginActivity.this, SweetAlertDialog.ERROR_TYPE)
                            .setContentText(message)
                            .show();
                }
            }

            @Override
            public void onFailure() {
                pDialog.dismiss();

                new SweetAlertDialog(LoginActivity.this, SweetAlertDialog.ERROR_TYPE)
                        .setContentText("Oops network error. Please try again.")
                        .setConfirmText("Try again")
                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sweetAlertDialog) {
                                login(phoneNumber, password);
                            }
                        })
                        .show();
            }
        });
    }
}
