package app.dummy.com.activities;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.google.android.material.navigation.NavigationView;

import app.dummy.com.R;
import app.dummy.com.fragments.BookmarksFragment;
import app.dummy.com.fragments.HomeFragment;
import app.dummy.com.fragments.MyArticlesFragment;
import app.dummy.com.retrofit.observers.RetrofitListener;
import app.dummy.com.room.models.User;
import app.dummy.com.viewmodels.ArticleViewModel;
import app.dummy.com.viewmodels.AuthViewModel;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    public static final String SHARED_PREFERENCES_KEY = "DUMMY-APP";

    public static final String IS_FIRST_LAUNCH = "IS_FIRST_LAUNCH";

    private AuthViewModel authViewModel;
    private SharedPreferences sharedPreferences;

    private TextView userNameTextView;
    private TextView userPhoneNumberTextView;
    private ArticleViewModel articleViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        NavigationView navigationView = findViewById(R.id.nav_view);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        navigationView.setNavigationItemSelectedListener(this);

        sharedPreferences = getSharedPreferences(SHARED_PREFERENCES_KEY, Context.MODE_PRIVATE);

        authViewModel = ViewModelProviders.of(this).get(AuthViewModel.class);
        articleViewModel = ViewModelProviders.of(this).get(ArticleViewModel.class);

        if (isFirstLaunch()) {
            startActivity(new Intent(this, IdentificationActivity.class));
            finish();
        }

        userNameTextView = navigationView.getHeaderView(0).findViewById(R.id.user_name);
        userPhoneNumberTextView = navigationView.getHeaderView(0).findViewById(R.id.user_phone_number);

        authViewModel.findCurrentUser().observe(this, new Observer<User>() {
            @Override
            public void onChanged(User user) {
                userNameTextView.setText(user.getFullName());
                userPhoneNumberTextView.setText(user.getPhoneNumber());
            }
        });

        articleViewModel.fetchArticles(null).observe(new RetrofitListener() {
            @Override
            public void onSuccess() {

            }
        });

        getSupportFragmentManager().beginTransaction().replace(R.id.fragmentContainer, new HomeFragment()).commit();
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_home) {
            getSupportFragmentManager().beginTransaction().replace(R.id.fragmentContainer, HomeFragment.newInstance()).commit();
        } else if (id == R.id.nav_bookmarks) {
            getSupportFragmentManager().beginTransaction().replace(R.id.fragmentContainer, BookmarksFragment.newInstance()).commit();
        } else if (id == R.id.nav_my_articles) {
            getSupportFragmentManager().beginTransaction().replace(R.id.fragmentContainer, MyArticlesFragment.newInstance()).commit();
        } else if (id == R.id.nav_profile) {

        }

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private boolean isFirstLaunch() {
        return sharedPreferences.getBoolean(IS_FIRST_LAUNCH, true);
    }
}
