package app.dummy.com.repositories;

import android.app.Application;

import androidx.lifecycle.LiveData;

import java.util.List;

import app.dummy.com.retrofit.ApiService;
import app.dummy.com.retrofit.Client;
import app.dummy.com.retrofit.observers.RetrofitObserver;
import app.dummy.com.retrofit.responses.ArticleResponse;
import app.dummy.com.retrofit.responses.ArticlesResponse;
import app.dummy.com.retrofit.responses.CommentArticleResponse;
import app.dummy.com.retrofit.responses.CommentsResponse;
import app.dummy.com.room.AppDatabase;
import app.dummy.com.room.asynctasks.InsertArticleAsyncTask;
import app.dummy.com.room.asynctasks.InsertCommentAsyncTask;
import app.dummy.com.room.asynctasks.InsertUserAsyncTask;
import app.dummy.com.room.models.Article;
import app.dummy.com.room.models.Comment;
import retrofit2.Call;
import retrofit2.Callback;

public class ArticleRepository {
    private static volatile ArticleRepository INSTANCE;

    private AppDatabase appDatabase;
    private ApiService apiService = Client.getClient();

    private ArticleRepository(Application application) {
        this.appDatabase = AppDatabase.getDatabase(application);
    }

    public static ArticleRepository getInstance(Application application) {
        if (INSTANCE == null) {
            synchronized (ArticleRepository.class) {
                if (INSTANCE == null) {
                    INSTANCE = new ArticleRepository(application);
                }
            }
        }

        return INSTANCE;
    }

    public LiveData<List<Article>> findAll() {
        return appDatabase.articleDao().findAll();
    }

    public LiveData<Article> find(Long articleId) {
        return appDatabase.articleDao().find(articleId);
    }

    public LiveData<List<Article>> findBookmarked(Long userId) {
        return appDatabase.articleDao().findBookmarked("%" + userId + "%");
    }

    public LiveData<List<Article>> findMyArticle(Long userId) {
        return appDatabase.articleDao().findMyArticle(userId);
    }

    public LiveData<List<Comment>> findCommentsForArticle(Long articleId) {
        return appDatabase.commentDao().findForArticle(articleId);
    }

    public RetrofitObserver fetchArticles(String atPage) {
        final RetrofitObserver retrofitObserver = new RetrofitObserver();

        apiService.fetchArticles(atPage).enqueue(new Callback<ArticlesResponse>() {
            @Override
            public void onResponse(Call<ArticlesResponse> call, retrofit2.Response<ArticlesResponse> response) {
                if (response.body() != null) {
                    for (Article article :
                            response.body().getArticles()) {
                        (new InsertArticleAsyncTask(appDatabase.articleDao())).execute(article);
                    }

                    retrofitObserver.retrofitListener.onSuccess();
                }
            }

            @Override
            public void onFailure(Call<ArticlesResponse> call, Throwable t) {
                retrofitObserver.retrofitListener.onFailure();
            }
        });

        return retrofitObserver;
    }

    public RetrofitObserver createArticle(String phoneNumber, String encryptedPassword, String articleTitle, String articleDescription, String articleImageUrl) {
        final RetrofitObserver retrofitObserver = new RetrofitObserver();

        apiService.createArticle(phoneNumber, encryptedPassword, articleTitle, articleDescription, articleImageUrl).enqueue(new Callback<ArticleResponse>() {
            @Override
            public void onResponse(Call<ArticleResponse> call, retrofit2.Response<ArticleResponse> response) {
                if (response.body() != null) {
                    if (response.body().getResponseStatus() == 200) {
                        (new InsertArticleAsyncTask(appDatabase.articleDao())).execute(response.body().getArticle());
                        (new InsertUserAsyncTask(appDatabase.userDao())).execute(response.body().getUser());
                    }

                    retrofitObserver.retrofitListener.onSuccess(response.body().getResponseStatus(), response.body().getMessage());
                }
            }

            @Override
            public void onFailure(Call<ArticleResponse> call, Throwable t) {
                retrofitObserver.retrofitListener.onFailure();
            }
        });

        return retrofitObserver;
    }

    public RetrofitObserver commentArticle(String phoneNumber, String encryptedPassword, Long articleId, String commentContent) {
        final RetrofitObserver retrofitObserver = new RetrofitObserver();

        apiService.commentArticle(phoneNumber, encryptedPassword, articleId, commentContent).enqueue(new Callback<CommentArticleResponse>() {
            @Override
            public void onResponse(Call<CommentArticleResponse> call, retrofit2.Response<CommentArticleResponse> response) {
                if (response.body() != null) {
                    if (response.body().getResponseStatus() == 200) {
                        (new InsertArticleAsyncTask(appDatabase.articleDao())).execute(response.body().getArticle());
                        (new InsertCommentAsyncTask(appDatabase.commentDao())).execute(response.body().getComment());
                    }

                    retrofitObserver.retrofitListener.onSuccess(response.body().getResponseStatus(), response.body().getMessage());
                }
            }

            @Override
            public void onFailure(Call<CommentArticleResponse> call, Throwable t) {
                retrofitObserver.retrofitListener.onFailure();
            }
        });

        return retrofitObserver;
    }

    public RetrofitObserver bookmarkUnBookmarkArticle(String phoneNumber, String encryptedPassword, Long articleId) {
        final RetrofitObserver retrofitObserver = new RetrofitObserver();

        apiService.bookmarkUnBookmarkArticle(phoneNumber, encryptedPassword, articleId).enqueue(new Callback<ArticleResponse>() {
            @Override
            public void onResponse(Call<ArticleResponse> call, retrofit2.Response<ArticleResponse> response) {
                if (response.body() != null) {
                    if (response.body().getResponseStatus() == 200) {
                        (new InsertArticleAsyncTask(appDatabase.articleDao())).execute(response.body().getArticle());
                    }

                    retrofitObserver.retrofitListener.onSuccess();
                }
            }

            @Override
            public void onFailure(Call<ArticleResponse> call, Throwable t) {
                retrofitObserver.retrofitListener.onFailure();
            }
        });

        return retrofitObserver;
    }

    public RetrofitObserver likeDislikeArticle(String phoneNumber, String encryptedPassword, Long articleId) {
        final RetrofitObserver retrofitObserver = new RetrofitObserver();

        apiService.likeDislikeArticle(phoneNumber, encryptedPassword, articleId).enqueue(new Callback<ArticleResponse>() {
            @Override
            public void onResponse(Call<ArticleResponse> call, retrofit2.Response<ArticleResponse> response) {
                if (response.body() != null) {
                    if (response.body().getResponseStatus() == 200) {
                        (new InsertArticleAsyncTask(appDatabase.articleDao())).execute(response.body().getArticle());
                    }

                    retrofitObserver.retrofitListener.onSuccess();
                }
            }

            @Override
            public void onFailure(Call<ArticleResponse> call, Throwable t) {
                retrofitObserver.retrofitListener.onFailure();
            }
        });

        return retrofitObserver;
    }

    public RetrofitObserver fetchUserBookmarks(String phoneNumber, String encryptedPassword) {
        final RetrofitObserver retrofitObserver = new RetrofitObserver();

        apiService.fetchUserBookmarks(phoneNumber, encryptedPassword).enqueue(new Callback<ArticlesResponse>() {
            @Override
            public void onResponse(Call<ArticlesResponse> call, retrofit2.Response<ArticlesResponse> response) {
                if (response.body() != null) {
                    if (response.body().getResponseStatus() == 200) {
                        for (Article article :
                                response.body().getArticles()) {
                            (new InsertArticleAsyncTask(appDatabase.articleDao())).execute(article);
                        }
                    }

                    retrofitObserver.retrofitListener.onSuccess();
                }
            }

            @Override
            public void onFailure(Call<ArticlesResponse> call, Throwable t) {
                retrofitObserver.retrofitListener.onFailure();
            }
        });

        return retrofitObserver;
    }

    public RetrofitObserver fetchArticleComments(Long articleId, String atPage) {
        final RetrofitObserver retrofitObserver = new RetrofitObserver();

        apiService.fetchArticleComments(articleId, atPage).enqueue(new Callback<CommentsResponse>() {
            @Override
            public void onResponse(Call<CommentsResponse> call, retrofit2.Response<CommentsResponse> response) {
                if (response.body() != null) {
                    if (response.body().getResponseStatus() == 200) {
                        for (Comment comment : response.body().getComments()) {
                            (new InsertCommentAsyncTask(appDatabase.commentDao())).execute(comment);
                        }
                    }

                    retrofitObserver.retrofitListener.onSuccess();
                }
            }

            @Override
            public void onFailure(Call<CommentsResponse> call, Throwable t) {
                retrofitObserver.retrofitListener.onFailure();
            }
        });

        return retrofitObserver;
    }
}
