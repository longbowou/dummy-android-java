package app.dummy.com.repositories;

import android.app.Application;

import androidx.lifecycle.LiveData;

import app.dummy.com.retrofit.ApiService;
import app.dummy.com.retrofit.Client;
import app.dummy.com.retrofit.observers.RetrofitObserver;
import app.dummy.com.retrofit.responses.Response;
import app.dummy.com.retrofit.responses.UserResponse;
import app.dummy.com.room.AppDatabase;
import app.dummy.com.room.asynctasks.InsertUserAsyncTask;
import app.dummy.com.room.models.User;
import retrofit2.Call;
import retrofit2.Callback;


public class AuthRepository {
    private static volatile AuthRepository INSTANCE;

    private AppDatabase appDatabase;
    private ApiService apiService = Client.getClient();

    private AuthRepository(Application application) {
        this.appDatabase = AppDatabase.getDatabase(application);
    }

    public static AuthRepository getInstance(Application application) {
        if (INSTANCE == null) {
            synchronized (AuthRepository.class) {
                if (INSTANCE == null) {
                    INSTANCE = new AuthRepository(application);
                }
            }
        }

        return INSTANCE;
    }

    public LiveData<User> findCurrentUser() {
        return appDatabase.userDao().findCurrentUser();
    }

    public RetrofitObserver identification(String phoneNumber) {
        final RetrofitObserver retrofitObserver = new RetrofitObserver();

        apiService.identification(phoneNumber).enqueue(new Callback<Response>() {
            @Override
            public void onResponse(Call<Response> call, retrofit2.Response<Response> response) {
                if (response.body() != null) {
                    retrofitObserver.retrofitListener.onSuccess(response.body().getResponseStatus(), response.body().getMessage());
                }
            }

            @Override
            public void onFailure(Call<Response> call, Throwable t) {
                retrofitObserver.retrofitListener.onFailure();
            }
        });

        return retrofitObserver;
    }

    public RetrofitObserver register(String phoneNumber, String fullName, String password, String passwordConfirmation) {
        final RetrofitObserver retrofitObserver = new RetrofitObserver();

        apiService.register(phoneNumber, fullName, password, passwordConfirmation).enqueue(new Callback<UserResponse>() {
            @Override
            public void onResponse(Call<UserResponse> call, retrofit2.Response<UserResponse> response) {
                if (response.body() != null) {
                    if (response.body().getResponseStatus() == 200) {
                        (new InsertUserAsyncTask(appDatabase.userDao())).execute(response.body().getUser());
                    }

                    retrofitObserver.retrofitListener.onSuccess(response.body().getResponseStatus(), response.body().getMessage());
                }
            }

            @Override
            public void onFailure(Call<UserResponse> call, Throwable t) {
                retrofitObserver.retrofitListener.onFailure();
            }
        });

        return retrofitObserver;
    }

    public RetrofitObserver login(String phoneNumber, String password) {
        final RetrofitObserver retrofitObserver = new RetrofitObserver();

        apiService.login(phoneNumber, password).enqueue(new Callback<UserResponse>() {
            @Override
            public void onResponse(Call<UserResponse> call, retrofit2.Response<UserResponse> response) {
                if (response.body() != null) {
                    if (response.body().getResponseStatus() == 200) {
                        (new InsertUserAsyncTask(appDatabase.userDao())).execute(response.body().getUser());
                    }

                    retrofitObserver.retrofitListener.onSuccess(response.body().getResponseStatus(), response.body().getMessage());
                }
            }

            @Override
            public void onFailure(Call<UserResponse> call, Throwable t) {
                retrofitObserver.retrofitListener.onFailure();
            }
        });

        return retrofitObserver;
    }
}
