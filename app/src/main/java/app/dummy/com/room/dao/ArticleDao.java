package app.dummy.com.room.dao;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import java.util.List;

import app.dummy.com.room.models.Article;

@Dao
public interface ArticleDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(Article article);

    @Query("DELETE FROM articles")
    void deleteAll();

    @Query("SELECT * from articles ORDER BY created_at DESC")
    LiveData<List<Article>> findAll();

    @Query("SELECT * from articles WHERE id = :articleId LIMIT 1")
    LiveData<Article> find(Long articleId);

    @Query("SELECT * from articles WHERE users_bookmarks_ids LIKE :userId ORDER BY created_at DESC")
    LiveData<List<Article>> findBookmarked(String userId);

    @Query("SELECT * from articles WHERE user_id = :userId ORDER BY created_at DESC")
    LiveData<List<Article>> findMyArticle(Long userId);

}
