package app.dummy.com.room.asynctasks;

import android.os.AsyncTask;

import app.dummy.com.room.dao.ArticleDao;
import app.dummy.com.room.dao.CommentDao;
import app.dummy.com.room.models.Article;
import app.dummy.com.room.models.Comment;

public class InsertCommentAsyncTask extends AsyncTask<Comment, Void, Void> {

    private CommentDao commentDao;

    public InsertCommentAsyncTask(CommentDao commentDao) {
        this.commentDao = commentDao;
    }

    @Override
    protected Void doInBackground(Comment... params) {
        commentDao.insert(params[0]);
        return null;
    }
}
