package app.dummy.com.room.models;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import com.google.gson.annotations.SerializedName;

@Entity(tableName = "users")
public class User {

    @PrimaryKey
    private Long id;

    @ColumnInfo(name = "full_name")
    @SerializedName("full_name")
    private String fullName;

    @ColumnInfo(name = "phone_number")
    @SerializedName("phone_number")
    private String phoneNumber;

    private String email;

    @ColumnInfo(name = "article_count")
    @SerializedName("article_count")
    private int articleCount;

    @ColumnInfo(name = "created_at")
    @SerializedName("created_at")
    private Long createdAt;

    @ColumnInfo(name = "encrypted_password")
    @SerializedName("encrypted_password")
    private String encryptedPassword;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public int getArticleCount() {
        return articleCount;
    }

    public void setArticleCount(int articleCount) {
        this.articleCount = articleCount;
    }

    public Long getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Long createdAt) {
        this.createdAt = createdAt;
    }

    public String getEncryptedPassword() {
        return encryptedPassword;
    }

    public void setEncryptedPassword(String encryptedPassword) {
        this.encryptedPassword = encryptedPassword;
    }
}
