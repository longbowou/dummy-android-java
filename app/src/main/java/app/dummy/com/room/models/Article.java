package app.dummy.com.room.models;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import com.google.gson.annotations.SerializedName;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;

@Entity(tableName = "articles")
public class Article {

    @PrimaryKey
    private Long id;

    private String title;

    private String description;

    private String image;

    @ColumnInfo(name = "user_id")
    @SerializedName("user_id")
    private Long userId;

    @ColumnInfo(name = "created_at")
    @SerializedName("created_at")
    private Long createdAt;

    @ColumnInfo(name = "likes_count")
    @SerializedName("likes_count")
    private int likesCount;

    @ColumnInfo(name = "users_likes_ids")
    @SerializedName("users_likes_ids")
    private String usersLikesIds;

    @ColumnInfo(name = "comments_count")
    @SerializedName("comments_count")
    private int commentsCount;

    private String author;

    @ColumnInfo(name = "users_bookmarks_ids")
    @SerializedName("users_bookmarks_ids")
    private String usersBookmarksIds;

    public String getCreatedAtLabel() {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("MMM dd y");
        return simpleDateFormat.format(new Date(createdAt));
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Long getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Long createdAt) {
        this.createdAt = createdAt;
    }

    public int getLikesCount() {
        return likesCount;
    }

    public void setLikesCount(int likesCount) {
        this.likesCount = likesCount;
    }

    public String getUsersLikesIds() {
        return usersLikesIds;
    }

    public void setUsersLikesIds(String usersLikesIds) {
        this.usersLikesIds = usersLikesIds;
    }

    public int getCommentsCount() {
        return commentsCount;
    }

    public void setCommentsCount(int commentsCount) {
        this.commentsCount = commentsCount;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getUsersBookmarksIds() {
        return usersBookmarksIds;
    }

    public void setUsersBookmarksIds(String usersBookmarksIds) {
        this.usersBookmarksIds = usersBookmarksIds;
    }
}
