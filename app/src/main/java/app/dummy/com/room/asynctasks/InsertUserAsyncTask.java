package app.dummy.com.room.asynctasks;

import android.os.AsyncTask;

import app.dummy.com.room.dao.UserDao;
import app.dummy.com.room.models.User;

public class InsertUserAsyncTask extends AsyncTask<User, Void, Void> {
    private UserDao userDao;

    public InsertUserAsyncTask(UserDao userDao) {
        this.userDao = userDao;
    }

    @Override
    protected Void doInBackground(User... params) {
        userDao.insert(params[0]);
        return null;
    }
}
