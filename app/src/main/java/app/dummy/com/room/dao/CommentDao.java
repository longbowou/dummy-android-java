package app.dummy.com.room.dao;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import java.util.List;

import app.dummy.com.room.models.Article;
import app.dummy.com.room.models.Comment;

@Dao
public interface CommentDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(Comment comment);

    @Query("DELETE FROM comments")
    void deleteAll();

    @Query("SELECT * from comments WHERE article_id = :articleId ORDER BY created_at DESC")
    LiveData<List<Comment>> findForArticle(Long articleId);
}
