package app.dummy.com.room.asynctasks;

import android.os.AsyncTask;

import app.dummy.com.room.dao.ArticleDao;
import app.dummy.com.room.dao.UserDao;
import app.dummy.com.room.models.Article;
import app.dummy.com.room.models.User;

public class InsertArticleAsyncTask extends AsyncTask<Article, Void, Void> {
    private ArticleDao articleDao;

    public InsertArticleAsyncTask(ArticleDao articleDao) {
        this.articleDao = articleDao;
    }

    @Override
    protected Void doInBackground(Article... params) {
        articleDao.insert(params[0]);
        return null;
    }
}
