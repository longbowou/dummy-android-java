package app.dummy.com.room;


import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

import app.dummy.com.room.dao.ArticleDao;
import app.dummy.com.room.dao.CommentDao;
import app.dummy.com.room.dao.UserDao;
import app.dummy.com.room.models.Article;
import app.dummy.com.room.models.Comment;
import app.dummy.com.room.models.User;

@Database(entities = {User.class, Article.class, Comment.class}, version = 1)
public abstract class AppDatabase extends RoomDatabase {

    private static volatile AppDatabase INSTANCE;

    public abstract UserDao userDao();

    public abstract ArticleDao articleDao();

    public abstract CommentDao commentDao();

    public static AppDatabase getDatabase(final Context context) {
        if (INSTANCE == null) {
            synchronized (AppDatabase.class) {
                if (INSTANCE == null) {
                    INSTANCE = Room.databaseBuilder(context.getApplicationContext(),
                            AppDatabase.class, "dummy_api_database")
                            .build();
                }
            }
        }
        return INSTANCE;
    }
}
